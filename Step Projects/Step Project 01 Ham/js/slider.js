const track = document.querySelector('.peopleSay-list');

const slides = Array.from(document.querySelectorAll('.peopleSay-elem'));
// console.log(slides.length);
const nextButton = document.getElementById('button-next');
const prevButton = document.getElementById('button-prev');

const tabNav = document.querySelector('.peopleSay-tab');
const tabImgs = Array.from(document.querySelectorAll('.peopleSay-tab-elem'));

const slideWidth = slides[0].getBoundingClientRect().width;

const setSlidePosition = (slide, index) => {
    slide.style.left = slideWidth * index + 'px';
}

slides.forEach(setSlidePosition);

const moveToSlide = (track, currentSlide, targetSlide) => {
    track.style.transform = 'translateX(-'+ targetSlide.style.left +')';
    currentSlide.classList.remove('current-slide');
    targetSlide.classList.add('current-slide');
}

const changeCurrentMiniImg = (currentMiniImg, targetTab) => {
    currentMiniImg.classList.remove('active');
    targetTab.classList.add('active');
}

prevButton.addEventListener('click', event => {
    const currentSlide = track.querySelector('.current-slide');
    const indexCurrentSlide = slides.indexOf(currentSlide);
    let prevSlide = slides[indexCurrentSlide-1];
    let indexPrevSlide = slides.indexOf(prevSlide);
    if (indexPrevSlide < 0){
        prevSlide = slides[slides.length-1];
    }

    const currentMiniImg = tabNav.querySelector('.active');
    const indexCurrentMiniImg = tabImgs.indexOf(currentMiniImg);
    let prevMiniImg = tabImgs[indexCurrentMiniImg-1];
    let indexPrevMiniImg = tabImgs.indexOf(prevMiniImg);
    if (indexPrevMiniImg < 0){
        prevMiniImg = tabImgs[tabImgs.length-1];
    }
     moveToSlide(track, currentSlide, prevSlide);
    changeCurrentMiniImg(currentMiniImg, prevMiniImg);

})

nextButton.addEventListener('click', event => {
    const currentSlide = track.querySelector('.current-slide');
    const indexCurrentSlide = slides.indexOf(currentSlide);
    let nextSlide = slides[indexCurrentSlide+1];
    let indexNextSlide = slides.indexOf(nextSlide);
    if (indexCurrentSlide > 2){
        indexNextSlide = 0;
        nextSlide = slides[indexNextSlide];
    }

    const currentMiniImg = tabNav.querySelector('.active');
    const indexCurrentMiniImg = tabImgs.indexOf(currentMiniImg);
    let nextMiniImg = tabImgs[indexCurrentMiniImg+1];
    let indexNextMiniImg = tabImgs.indexOf(nextMiniImg);
    if (indexCurrentMiniImg > 2){
        indexNextMiniImg = 0;
        nextMiniImg = tabImgs[indexNextMiniImg];
    }
    moveToSlide(track, currentSlide, nextSlide);
    changeCurrentMiniImg(currentMiniImg, nextMiniImg);
})

tabNav.addEventListener('click', event => {
    const targetTab = event.target.closest('li');

    if (!targetTab) return;

    const currentSlide = track.querySelector('.current-slide');
    const currentMiniImg = tabNav.querySelector('.active');
    const targetIndex = tabImgs.findIndex(img => img === targetTab);

    const targetSlide = slides[targetIndex];
    moveToSlide(track, currentSlide, targetSlide);
    changeCurrentMiniImg(currentMiniImg, targetTab);

})
