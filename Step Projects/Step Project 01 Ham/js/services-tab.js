const services = document.querySelectorAll('.services-item');
const servicesInfo = document.querySelectorAll('.services-item-info')

services.forEach(elem => {
    elem.addEventListener('click', function(event){
        document.querySelector('.services-item.active').classList.remove('active');
        event.target.classList.add('active');

        const textIndex = event.target.getAttribute('data-info');

        servicesInfo.forEach(item => {
            item.classList.add('hidden');
            if (item.getAttribute('data-info') === textIndex){
                item.classList.remove("hidden");
            }
        })
    })
})
