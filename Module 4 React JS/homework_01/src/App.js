import React, { Component } from "react";
import "./App.css";
import Button from "./Button/Button";
import Modal from "./Modal/Modal";

class App extends Component {
  state = {
    isOpenFirst: false,
    isOpenSecond: false,
  };

  openModal = (isSecond = false) => {
    if (isSecond) {
      this.setState({ isOpenSecond: true });
    } else {
      this.setState({ isOpenFirst: true });
    }
  };

  closeModal = (isSecond = false) => {
    if (isSecond) {
      this.setState({ isOpenSecond: false });
    } else {
      this.setState({ isOpenFirst: false });
    }
  };
  render() {
    const { isOpenFirst, isOpenSecond } = this.state;
    const modal1 = {
      header: "Do you want to delete this file?",
      closeButton: true,
      text: "Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?",
    };
    const modal2 = {
      header: "Title of Modal 2",
      closeButton: false,
      text: "Content of Modal 2",
    };

    const actionsModal1 = (
      <>
        <button className="modal__button modal1__button modal1__button--first">
          AGREE
        </button>
        <button
          className="modal__button modal1__button modal1__button--second"
          onClick={() => this.closeModal()}
        >
          CLOSE MODAL
        </button>
      </>
    );
    const actionsModal2 = (
      <>
        <button className="modal__button modal2__button modal2__button--first">
          SURE!
        </button>
        <button
          className="modal__button modal2__button modal2__button--first"
          onClick={() => this.closeModal(true)}
        >
          CLOSE
        </button>
      </>
    );

    return (
      <>
        {isOpenFirst && (
          <div className="modal">
            <div onClick={() => this.closeModal(true)} className="background" />
            <Modal
              header={modal1.header}
              closeButton={modal1.closeButton}
              text={modal1.text}
              actions={actionsModal1}
              isOpenFirst={this.state.isOpenFirst}
              isOpenSecond={this.state.isOpenSecond}
              closeModal={this.closeModal}
              className="content modalFirst"
            />
          </div>
        )}

        {isOpenSecond && (
          <div className="modal">
            <div onClick={() => this.closeModal(true)} className="background" />
            <Modal
              header={modal2.header}
              closeButton={modal2.closeButton}
              text={modal2.text}
              actions={actionsModal2}
              isOpenFirst={this.state.isOpenFirst}
              isOpenSecond={this.state.isOpenSecond}
              closeModal={() => this.closeModal(true)}
              className="content modalSecond"
            />
          </div>
        )}
        <Button
          onClick={() => this.openModal()}
          backgroundColor="pink"
          text="Open first modal"
        />
        <Button
          onClick={() => this.openModal(true)}
          backgroundColor="#abe4f8"
          text="Open second modal"
        />
      </>
    );
  }
}

export default App;
