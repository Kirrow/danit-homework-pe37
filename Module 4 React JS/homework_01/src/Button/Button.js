import React, { Component } from "react";
import styles from "./button.scss"

class Button extends Component {
  render() {
    const { backgroundColor, text, onClick } = this.props;
    console.log(this.props);
    return (
      <>
        <button style={{ backgroundColor: backgroundColor }} onClick={onClick} className="buttons__main">{text}</button>
      </>
    );
  }
}

export default Button;
