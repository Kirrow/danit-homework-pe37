import React, { Component } from "react";
import styles from'./modal.scss'

class Modal extends Component {
  render() {
    console.log(this.props);
    const { header, closeButton, text, actions, isOpenSecond, closeModal, className } =
      this.props;

    return (
      <>
          <div className="modal" onClick={() => closeModal(isOpenSecond)}>
            <div className={className} onClick={(e) => e.stopPropagation()}>
                <div className="modal__header">
                <h1 className="modal__header--title">{header}</h1>
            {closeButton && (<button
                className="modal__header--close-btn"
                onClick={() => closeModal(isOpenSecond)}
              >
                X
              </button>)}
              </div>
                <p className="modal__text">{text}</p>
              <div className="modal__button-container">
                {actions}
              </div>
            </div>
          </div>
      </>
    );
  }
}

export default Modal;
