import React, {useEffect} from "react";
import "./App.css";
import Modal from "./components/Modal/Modal";
import GoodsContainer from "./components/GoodsContainer/GoodsContainer";
// import PropTypes from "prop-types";
import Header from "./components/Header/Header";
// import HomePage from "./pages/HomePage/HomePage";
import Routes from "./Routes/Routes";
import {useDispatch} from "react-redux";
import { fetchData } from "./store/actionCreators/dataAC";

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchData());
  }, []);

  return (
    
      <div className="App">
        <Header />
        <Modal />
        <section>
          {/* <GoodsContainer /> */}
          <Routes />
        </section>
      </div>
  );
}

export default App;
