import {
  FETCH_DATA,
  FETCH_DATA_FROM_LS,
  TOGGLE_ISFAVORITE,
  TOGGLE_ISINCART,
  DELETE_FROM_CART,
} from "../actions/dataAction";

export const fetchData = () => {
  return async dispatch => {
    const data = JSON.parse(localStorage.getItem("data"))
    // console.log(data);
    if (data) {
      dispatch({ type: FETCH_DATA_FROM_LS, payload: data });
      // console.log("data in LS", data);
    } else {
      const result = await fetch("./goods.json").then(res => res.json());
      console.log("data from goods.json", result);
      localStorage.setItem("data", JSON.stringify(result));
      dispatch({ type: FETCH_DATA, payload: result });
    }
  };
};

export const toggleIsFavorite = id => dispatch => {
  const data = JSON.parse(localStorage.getItem("data"));
  // console.log(data);
  const newData = [...data.data];
  console.log(newData);
  const index = newData.findIndex(item => item.id === id);
  if (index !== -1) {
    dispatch({ type: TOGGLE_ISFAVORITE, payload: id });
  }
};

export const toggleIsInCart = id => dispatch => {
  const data = JSON.parse(localStorage.getItem("data"));
  // console.log(data);
  const newData = [...data.data];
  const index = newData.findIndex(item => item.id === id);
  if (index !== -1) {
    dispatch({ type: TOGGLE_ISINCART, payload: id });
  }
};

export const deleteCartContainer = () => dispatch => {
  const data = JSON.parse(localStorage.getItem("data"));
  // console.log(data);
  const newData = [...data.data];
  newData.forEach(elem=>elem.isInCart = false)
  // console.log(newData);
  dispatch({ type: DELETE_FROM_CART, payload: newData })
};

// const {data} = await fetch('http://localhost:3001/todo',{
//     method:'PATCH',
//     headers:{
//         "content-type":"application/json"
//     },
//     body:JSON.stringify({id})
// })
//     .then(rsp => rsp.json());
// if(status === "success"){
//     dispatch({type: TOGGLE_ISDONE, payload: id})
// }
// }

// dispatch(setIsLoading(false))
// dispatch({type: FETCH_DATA, payload: data})
// };
