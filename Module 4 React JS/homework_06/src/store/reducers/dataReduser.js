import {
  FETCH_DATA,
  FETCH_DATA_FROM_LS,
  TOGGLE_ISFAVORITE,
  TOGGLE_ISINCART,
  DELETE_FROM_CART,
} from "../actions/dataAction";

const initialState = {
  data: null,
  // isFavorite: false,
  // isInCart: false,
};

const dataReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_DATA:
      const dataFetch = { ...state, data: action.payload };
      // console.log(data);

      localStorage.setItem("data", JSON.stringify(dataFetch));
      return dataFetch;

    case FETCH_DATA_FROM_LS:
      const {data} = { ...state, data: action.payload };
      // console.log("data in LS dataReducer", data);
      return data;

    // case ADD_NOTE:{

    //     localStorage.setItem('notes', JSON.stringify({...state, notes: [...state.notes, action.payload]}));
    //     return {...state, notes: [...state.notes, action.payload]};
    // }

    case TOGGLE_ISFAVORITE: {
      // console.log(state);
      const tempData = [...state.data];
      // console.log(state);

      const currIndex = tempData.findIndex(elem => elem.id === action.payload);
      tempData[currIndex].isFavorite = !tempData[currIndex].isFavorite;

      // console.log(tempData);
      localStorage.setItem("data", JSON.stringify({...state, data: tempData}));
      return { ...state, data: tempData };
    }

    case TOGGLE_ISINCART: {
      const tempData = [...state.data];

      const currIndex = tempData.findIndex(elem => elem.id === action.payload);
      tempData[currIndex].isInCart = !tempData[currIndex].isInCart;

      localStorage.setItem(
        "data",
        JSON.stringify({...state, data: tempData})
      );
      return { ...state, data: tempData };
    }

    case DELETE_FROM_CART: {
      // const tempData = [...state.data];

      // const currIndex = tempData.findIndex(elem => elem.id === action.payload);
      // tempData[currIndex].isInCart = !tempData[currIndex].isInCart;

      localStorage.setItem(
        "data",
        JSON.stringify({...state, data: action.payload})
      );
      return { ...state, data: action.payload };
    }

    // case SET_IS_LOADING:

    //     localStorage.setItem('notes', JSON.stringify({...state, isLoading: action.payload}));
    //     return {...state, isLoading: action.payload}

    // case CHANGE_TEXT:
    //     const tempNotes = [...state.notes];
    //     const currIndex = tempNotes.findIndex((elem) => elem.id === action.payload.id)
    //     tempNotes[currIndex].text = action.payload.text;

    //     localStorage.setItem('notes', JSON.stringify({...state, notes:tempNotes}));
    //     return {...state,notes:tempNotes}

    // case DELETE_NOTE:

    //     localStorage.setItem('notes', JSON.stringify({...state, notes: action.payload}));
    //     return {...state, notes: action.payload}

    default:
      return state;
  }
};
export default dataReducer;
