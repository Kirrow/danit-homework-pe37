import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import Cart from "../pages/Cart/Cart";
import Favorites from "../pages/Favorites/Favorites";
import HomePage from "../pages/HomePage/HomePage";

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/cart" component={Cart}></Route>

      <Route exact path="/favorites" component={Favorites}></Route>
      <Route exact path="/" component={HomePage}></Route>
      <Route exact path="/home">
        <Redirect to="/" />
      </Route>
    </Switch>
  );
};

export default Routes;
