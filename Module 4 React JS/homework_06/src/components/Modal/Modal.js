import React from "react";
import styles from "./Modal.module.scss";
// import {Button} from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { setIsOpenModalAC } from "../../store/actionCreators/modalAC";
import { toggleIsInCart } from "../../store/actionCreators/dataAC";

const Modal = () => {
  const { isOpen, config } = useSelector(({ modal }) => modal);

  const dispatch = useDispatch();
  if (!isOpen) {
    return null;
  }
  // console.log(config.id);
  return (
    <div
      className={styles.modal}
      onClick={() => {
        dispatch(setIsOpenModalAC(false));
      }}
    >
      <div className={styles.modalAddToCart} style={{backgroundColor: config.color }} onClick={e => e.stopPropagation()}>
        <div className={styles.modal__header}>
          <h1 className={styles.modalHeaderTitle}>{config.header}</h1>
          <button
            className={styles.modal__headerCloseBtn}
            onClick={() => {
              dispatch(setIsOpenModalAC(false));
            }}
          >
            X
          </button>
        </div>
        <p className={styles.modal__text}>{config.text}</p>
        <div className={styles.modal__buttonContainer}>
          <button
            className={styles.modal__button}
            onClick={() => {
              dispatch(toggleIsInCart(config.id));
              dispatch(setIsOpenModalAC(false));
            }}
          >
            {config.btnText}
          </button>
          <button
            className={styles.modal__buttonSecond}
            onClick={() => {
              dispatch(setIsOpenModalAC(false));
            }}
          >
            CANCEL
          </button>
        </div>
      </div>
    </div>

 
  );
};

// Modal.propTypes = {
//   id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
//   header: PropTypes.oneOfType([
//     PropTypes.string,
//     PropTypes.number,
//     PropTypes.symbol,
//   ]),
//   closeButton: PropTypes.bool,
//   text: PropTypes.oneOfType([
//     PropTypes.string,
//     PropTypes.number,
//     PropTypes.symbol,
//   ]),
//   closeModal: PropTypes.func,
//   className: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
//   addToCart: PropTypes.func,
// };

export default Modal;



