import React, { useState } from "react";
import Modal from "./Modal";
import {BrowserRouter} from "react-router-dom";
import {fireEvent, getByText, queryByTestId, queryByText, render, screen} from "@testing-library/react";
// import {useSelector} from 'react-redux';

const handleYesClick = jest.fn()

function Wrapper({isOpen}) {
    const [isModalOpen, setIsModalOpen] = useState(isOpen);

    return (
        <BrowserRouter>
            <button onClick={() => setIsModalOpen(true)}>open Modal</button>
            <Modal isOpen={isModalOpen} setIsOpen={setIsModalOpen} handleYesClick={handleYesClick}/>
        </BrowserRouter>
    )
}

Wrapper.defaultProps = {
    isOpen: false
}

describe('Modal will render', () => {
    test('should modal will open', () => {
        const {asFragment} = render(<Wrapper isOpen={true}/>)
        expect(asFragment()).toMatchSnapshot()
    })
    // test('should modal be closed', () => {
    //     const {asFragment} = render(<Wrapper isOpen={false}/>)
    //     expect(asFragment()).toMatchSnapshot()
    // })
})
