import React from 'react';
import PropTypes from 'prop-types';
import styles from './Header.module.scss';
import { NavLink } from "react-router-dom";

const Header = (props) => {
    const {} = props;

    return (
        <header className={styles.header}>
            <nav >
                <ul>
                    <li>
                        <NavLink exact activeClassName={styles.active} to="/">Home</NavLink>
                    </li>
                    <li>
                        <NavLink exact activeClassName={styles.active} to="/favorites">Favorites</NavLink>
                    </li>
                    <li>
                        <NavLink exact activeClassName={styles.active} to="/cart">Cart</NavLink>
                    </li>
                </ul>
            </nav>
        </header>
    )
}

Header.propTypes = {};
Header.defaultProps = {};

export default Header;