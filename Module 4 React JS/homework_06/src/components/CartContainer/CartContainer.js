import React, {useEffect} from "react";
import CartItem from "../CartItem/CartItem";
import styles from "./CartContainer.module.scss";
import { useSelector } from "react-redux";
import CartBuyForm from "../CartBuyForm/CartBuyForm"

const CartContainer = () => {
  const goods = useSelector(({ data }) => data.data);

  return (
    <>
    <div className={styles.cartContainer}>
      {goods && goods.map(({ id, isInCart, ...args }) => {
        if (isInCart) {
          return (
            <CartItem
              key={id}
              id={id}
              isInCart={isInCart}
              {...args}
            />
          );
        }
      })}
    </div>
    {goods && <CartBuyForm />}
    </>
  );
};

export default CartContainer;
