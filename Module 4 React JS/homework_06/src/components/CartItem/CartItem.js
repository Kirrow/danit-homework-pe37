import React from "react";
import styles from "./CartItem.module.scss";
// import PropTypes from "prop-types";
import Button from "../Button/Button";
import { useDispatch } from "react-redux";
import {
  setIsOpenModalAC,
  setConfig,
} from "../../store/actionCreators/modalAC";

const CartItem = props => {
  // console.log(props);
  const { id, name, price, article, url } = props;
  const dispatch = useDispatch();
  const modalDeleteFromCart = {
    header: `Do you really want to delete ${name} from Cart?`,
    closeButton: true,
    text: `Later you can add ${name} to Cart again. Are you sure?`,
    btnText: "DELETE FROM CART",
    color: "#e74c3c",
  };

  return (
    <div className={styles.root}>
      <div className={styles.btn__cartDeleteWrapper}>
        <Button
          onClick={() => {
            dispatch(setIsOpenModalAC(true));
            dispatch(
              setConfig(
                id,
                name,
                modalDeleteFromCart.header,
                modalDeleteFromCart.text,
                modalDeleteFromCart.btnText,
                modalDeleteFromCart.color
              )
            );
          }}
          text="X"
          id={id}
          className={styles.btn__cartDelete}
        />
      </div>
      <div className={styles.goodImgWrapper}>
        <img src={url} alt={name} className={styles.goodImg} />
      </div>
      <h1 className={styles.name}>{name}</h1>
      <div className={styles.priceWrapper}>
        <h3>Цена: {price} грн</h3>
      </div>
      <p>Номер товара: {article}</p>
    </div>
  );
};

// Good.propTypes = {
//   id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
//   name: PropTypes.oneOfType([
//     PropTypes.string,
//     PropTypes.number,
//     PropTypes.symbol,
//   ]),
//   price: PropTypes.number.isRequired,
//   url: PropTypes.string,
//   article: PropTypes.number,
//   isFavorite: PropTypes.bool,
//   openModal: PropTypes.func,
//   setIsFavorite: PropTypes.func,
//   isInCart: PropTypes.bool,
//   changeId: PropTypes.func,
// };

// Good.defaultProps = {
//   id: "",
//   name: "",
//   price: "",
//   url: "",
//   article: "",
//   isFavorite: false,
//   openModal: () => {},
//   setIsFavorite: () => {},
//   isInCart: false,
//   changeId: () => {},
// };

export default CartItem;
