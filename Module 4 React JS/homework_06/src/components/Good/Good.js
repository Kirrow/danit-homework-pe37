import React from "react";
import styles from "./Good.module.scss";
import PropTypes from "prop-types";
import Button from "../Button/Button";
import { ReactComponent as StarPlus } from "../../assets/svg/starPlus.svg";
import { ReactComponent as Star } from "../../assets/svg/star.svg";
import { toggleIsFavorite } from "../../store/actionCreators/dataAC";
import { useDispatch } from "react-redux";
import { setIsOpenModalAC, setConfig } from "../../store/actionCreators/modalAC";

const Good = props => {
  // console.log(props);
  const {
    id,
    name,
    price,
    url,
    article,
    isFavorite,
    isInCart,
  } = props;
    const modalAddToCart = {
        header: `Do you really want to add ${name} to cart?`,
        closeButton: true,
        text: `Later you can delete ${name} from Cart. Are you sure you want to add it to CART?`,
        btnText: "ADD TO CART"
      };

  const dispatch = useDispatch();
  // console.log(props);

  return (
    <div className={styles.root}>
      {isFavorite ? (
        <div className={styles.svg__wrapper}>
          <div>
            <Star onClick={() => dispatch(toggleIsFavorite(id))} />
          </div>
        </div>
      ) : (
        <div className={styles.svg__wrapper}>
          <div>
            <StarPlus onClick={() => dispatch(toggleIsFavorite(id))} />
          </div>
        </div>
      )}
      <div className={styles.goodImgWrapper}>
        <img src={url} alt={name} className={styles.goodImg} />
      </div>
      <h1 className={styles.name}>{name}</h1>
      <div className={styles.priceWrapper}>
        <h3>Цена: {price} грн</h3>
        <div className={styles.btn__addCartWrapper}>
          <Button
            onClick={() => {
              dispatch(setIsOpenModalAC(true))
              dispatch(setConfig(id, name, modalAddToCart.header, modalAddToCart.text, modalAddToCart.btnText))
            }}
            backgroundColor="rgb(255, 243, 177)"
            text="ADD to CART"
            id={id}
            className={styles.btn__addCart}
          />
        </div>
      </div>
      <p>Номер товара: {article}</p>
      {isInCart ? <span>Is in Cart</span> : <span>Not in Cart</span>}
    </div>
  );
};

Good.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  name: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.symbol,
  ]),
  price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  url: PropTypes.string,
  article: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  isFavorite: PropTypes.bool,
  openModal: PropTypes.func,
  setIsFavorite: PropTypes.func,
  isInCart: PropTypes.bool,
  changeId: PropTypes.func,
};

Good.defaultProps = {
  id: "",
  name: "",
  price: "",
  url: "",
  article: "",
  isFavorite: false,
  openModal: () => {},
  setIsFavorite: () => {},
  isInCart: false,
  changeId: () => {},
};

export default Good;
