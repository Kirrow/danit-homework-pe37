import React, { PureComponent } from "react";
import styles from "./button.scss";
import PropTypes from "prop-types";

class Button extends PureComponent {
  render() {
    const { backgroundColor, text, id, changeId } = this.props;
    return (
      <>
        <button
          style={{ backgroundColor: backgroundColor }}
          onClick={() => {
            changeId(id);
          }}
          className="buttons__main"
        >
          {text}
        </button>
      </>
    );
  }
}

Button.propTypes = {
  backgroundColor: PropTypes.string.isRequired,
  text: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  changeId: PropTypes.func,
};


Button.defaultProps = {
  backgroundColor: "white",
  text: "",
  id: "",
  changeId: () => {},
};

export default Button;
