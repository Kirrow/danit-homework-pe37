import React, { PureComponent } from "react";
import Good from "../Good/Good";
import styles from "./GoodsContainer.module.scss";
import PropTypes from "prop-types";

class GoodsContainer extends PureComponent {
  render() {
    // console.log(this.props);
    const {
      goods,
      openModal,
      setIsFavorite,
      setIsNotFavorite,
      isInCart,
      changeId,
    } = this.props;
    // console.log(setIsFavorite);

    return (
      <section className={styles.root}>
        <h1>GOODS</h1>
        <div className={styles.GoodsContainer}>
          {goods.map(({ id, isFavorite, ...args }) => (
            <Good
              openModal={openModal}
              key={id}
              id={id}
              setIsFavorite={setIsFavorite}
              setIsNotFavorite={setIsNotFavorite}
              isFavorite={isFavorite}
              isInCart={isInCart}
              changeId={changeId}
              {...args}
            />
          ))}
        </div>
      </section>
    );
  }
}

GoodsContainer.propTypes = {
  goods: PropTypes.array,
  openModal: PropTypes.func,
  setIsFavorite: PropTypes.func,
  setIsNotFavorite: PropTypes.func,
  isInCart: PropTypes.bool,
  changeId: PropTypes.func,
};

GoodsContainer.propTypes = {
  goods: [],
  openModal: () => {},
  setIsFavorite: () => {},
  setIsNotFavorite: () => {},
  isInCart: false,
  changeId: () => {},
};

export default GoodsContainer;
