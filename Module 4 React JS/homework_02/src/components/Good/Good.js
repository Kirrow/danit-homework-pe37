import React, { PureComponent } from "react";
import styles from "./Good.module.scss";
import PropTypes from "prop-types";
import Button from "../Button/Button";
import { ReactComponent as StarPlus } from "../../assets/svg/starPlus.svg";
import { ReactComponent as Star } from "../../assets/svg/star.svg";

class Good extends PureComponent {
  render() {
    // console.log(this.props);
    const {
      id,
      name,
      price,
      url,
      article,
      isFavorite,
      openModal,
      setIsFavorite,
      setIsNotFavorite,
      isInCart,
      changeId,
    } = this.props;
    return (
      <div className={styles.root}>
        {isFavorite ? (
          <Star onClick={() => setIsNotFavorite(id)} />
        ) : (
          <StarPlus onClick={() => setIsFavorite(id)} />
        )}
        <div className={styles.goodImgWrapper}>
          <img src={url} alt={name} className={styles.goodImg} />
        </div>
        <h1 className={styles.name}>{name}</h1>
        <div className={styles.priceWrapper}>
          <h3>Цена: {price} грн</h3>
          <Button
            changeId={changeId}
            openModal={openModal}
            backgroundColor="rgb(255, 243, 177)"
            text="ADD to CART"
            id={id}
          />
        </div>
        <p>Номер товара: {article}</p>
        {isInCart ? <span>Is in Cart</span> : <span>Not in Cart</span>}
      </div>
    );
  }
}

Good.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  name: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.symbol,
  ]),
  price: PropTypes.number.isRequired,
  url: PropTypes.string,
  article: PropTypes.number,
  isFavorite: PropTypes.bool,
  openModal: PropTypes.func,
  setIsFavorite: PropTypes.func,
  isInCart: PropTypes.bool,
  changeId: PropTypes.func,
};

Good.defaultProps = {
  id: "",
  name: "",
  price: "",
  url: "",
  article: "",
  isFavorite: false,
  openModal: () => {},
  setIsFavorite: () => {},
  isInCart: false,
  changeId: () => {},
};

export default Good;
