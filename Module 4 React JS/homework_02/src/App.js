import React, { Component } from "react";
import "./App.css";
import Modal from "./components/Modal/Modal";
import GoodsContainer from "./components/GoodsContainer/GoodsContainer";
import PropTypes from 'prop-types';

class App extends Component {
  state = {
    isModalOpen: false,
    data: [],
    id: "",
  };

  openModal = () => {
    this.setState({ isModalOpen: true });
  };

  closeModal = () => {
    this.setState({ isModalOpen: false });
  };

  async componentDidMount() {
    const dataInLocalStorage = localStorage.getItem("data");
    const data = JSON.parse(dataInLocalStorage);
    if (data) {
      this.setState({ data: data });
    } else {
      const result = await fetch("./goods.json").then(res => res.json());
      localStorage.setItem("data", JSON.stringify(result));
      this.setState({ data: result });
    }
  }

  setIsFavorite = id => {
    const index = this.state.data.findIndex(item => item.id === id);

    this.setState(current => {
      const newData = [...current.data];
      newData[index].isFavorite = true;
      localStorage.setItem("data", JSON.stringify(newData));
      return { data: newData };
    });
  };

  setIsNotFavorite = id => {
    const index = this.state.data.findIndex(item => item.id === id);

    this.setState(current => {
      const newData = [...current.data];
      newData[index].isFavorite = false;
      localStorage.setItem("data", JSON.stringify(newData));
      return { data: newData };
    });
  };

  addToCart = id => {
    // console.log(id);
    const index = this.state.data.findIndex(item => item.id === id);
    // console.log(index);

    this.setState(current => {
      const newData = [...current.data];
      newData[index].isInCart = true;
      localStorage.setItem("data", JSON.stringify(newData));
      return { data: newData };
    });
    this.closeModal();
  };

  changeId = newId => {
    this.setState({ id: newId });
    this.openModal();
  };

  render() {
    const { isModalOpen, data, id } = this.state;
    // console.log(this.state);
    const modalAddToCart = {
      header: "Do you really want to add this item to cart?",
      closeButton: true,
      text: "Later you can delete this item from Cart. Are you sure you want to add it to CART?",
    };

    return (
      <>
        {isModalOpen && (
          <div className="modal">
            <div onClick={() => this.closeModal()} className="background" />
            <Modal
              header={modalAddToCart.header}
              closeButton={modalAddToCart.closeButton}
              text={modalAddToCart.text}
              isModalOpen={isModalOpen}
              closeModal={() => this.closeModal()}
              addToCart={() => this.addToCart(id)}
              id={id}
              className="modalAddToCart"
            />
          </div>
        )}

        <GoodsContainer
          goods={data}
          setIsFavorite={this.setIsFavorite}
          setIsNotFavorite={this.setIsNotFavorite}
          changeId={this.changeId}
          openModal={() => {
            this.openModal();
          }}
        />

      </>
    );
  }
}

App.propTypes = {
  isModalOpen: PropTypes.bool,
  data: PropTypes.array,
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
]),
}

App.defaultProps = {
  isModalOpen: false,
  data: [],
  id: ''
}

export default App;
