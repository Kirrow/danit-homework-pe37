import React from "react";
import FavoritesItem from "../FavoritesItem/FavoritesItem";
import styles from "./FavoritesContainer.module.scss";
import { useSelector } from "react-redux";

const FavoritesContainer = props => {
  // console.log(props);
  const goods = useSelector(({ data }) => data.data);
// console.log(goods.data);
  return (
    <div className={styles.favoritesContainer}>
      {goods && goods.map(({ id, isFavorite, ...args }) => {
        if (isFavorite) {
          return (
            <FavoritesItem
              key={id}
              id={id}
            //   isInCart={isInCart}
              {...args}
            />
          );
        }
      })}
    </div>
  );
};

export default FavoritesContainer;
