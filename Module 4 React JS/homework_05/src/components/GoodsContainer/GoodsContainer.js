import React, { useEffect } from "react";
import PropTypes from "prop-types";
import Good from "../Good/Good";
import { useDispatch, useSelector } from "react-redux";
// import {fetchDataFromLS} from "../../store/actionCreators/noteAC";
import { fetchData } from "../../store/actionCreators/dataAC";
import {FETCH_DATA} from "../../store/actions/dataAction";
import styles from "./GoodsContainer.module.scss";

const GoodsContainer = () => {
  const data = useSelector((data) => data);
  // const isLoading = useSelector(({ notes }) => notes.isLoading);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchData());
  }, []);

  console.log("data", data);

  if (!data)
    return <p style={{ fontSize: 24 }}>You don't have any vegetables yet</p>;
  // if (isLoading) return <h1>Loading...</h1>;

  return (
    
    <section className={styles.root}>
      <h1>GOODS</h1>
      <div className={styles.GoodsContainer}>
        {data && data.map(({ id, isFavorite, ...args }) => (
          <Good
            key={id}
            id={id}
            {...args}
          />
        ))}
      </div>
    </section>
  );
};



GoodsContainer.propTypes = {
  data: PropTypes.array,
};
GoodsContainer.defaultProps = {
  data: null,
};

export default GoodsContainer;


// GoodsContainer.propTypes = {
//   goods: PropTypes.array,
//   openModal: PropTypes.func,
//   setIsFavorite: PropTypes.func,
//   setIsNotFavorite: PropTypes.func,
//   isInCart: PropTypes.bool,
//   changeId: PropTypes.func,
// };

// GoodsContainer.propTypes = {
//   goods: [],
//   openModal: () => {},
//   setIsFavorite: () => {},
//   setIsNotFavorite: () => {},
//   isInCart: false,
//   changeId: () => {},
// };

// export default GoodsContainer;
