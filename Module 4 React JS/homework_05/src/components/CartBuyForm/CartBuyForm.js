import React from "react";
import { Formik, Form } from "formik";
import * as yup from "yup";
import CustomInput from "../CustomInput/CustomInput";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { deleteCartContainer } from "../../store/actionCreators/dataAC";
import styles from "./CartBuyForm"

function CartBuyForm() {
  const cartData = useSelector(({ data }) => data.data).filter(
    item => item.isInCart
  );

  // console.log(names);
  // console.log(cartData);
  const history = useHistory();
  const dispatch = useDispatch();
  const initialValues = {
    name: "",
    surname: "",
    age: "",
    address: "",
    phone: "",
  };

  const onSubmit = (values, { resetForm }) => {
    console.log("Покупатель купил:");
    console.log(cartData);
    console.log("Контакты покупателя:");
    console.log(values);
    dispatch(deleteCartContainer());

    history.push("/");
    resetForm();
  }

    const validationSchema = yup.object().shape({
      name: yup
        .string()
        .required("Поле обязательно")
        .matches(/[A-Za-z ]/gi, "Только латинские"),
      surname: yup
        .string()
        .min(2, "Too Short!")
        .max(50, "Too Long!")
        .required("Поле обязательно")
        .matches(/[A-Za-z ]/gi, "Только латинские"),
      age: yup.string().required("Поле обязательно").matches(/^[0-9]/gi, "Только цифры"),
      address: yup.string().required("Поле обязательно").matches(/[A-Za-z ]/gi, "Только латинские"),
      phone: yup.string().required("Поле обязательно").matches(/[A-Za-z ]/gi, "Только латинские"),
    });
  

    return (
      <div className={styles.root}>
        <h1>Information about buyer</h1>
        <Formik
          initialValues={initialValues}
          onSubmit={onSubmit}
          validationSchema={validationSchema}
          
        >
          <Form >
            <CustomInput name="name" type="text" placeholder="Name" />
            <CustomInput name="surname" type="text" placeholder="Surname" />
            <CustomInput name="age" type="number" placeholder="Age" />
            <CustomInput name="address" type="text" placeholder="Address" />
            <CustomInput name="phone" type="tel" placeholder="Phone" />
            <button type={"submit"}>Checkout</button>
          </Form>
        </Formik>
      </div>
    );

}

export default CartBuyForm;
