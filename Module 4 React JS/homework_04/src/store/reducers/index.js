import { combineReducers } from "redux";
import modalReducer from "./modalReducer";
import dataReducer from "./dataReduser";

const reducer = combineReducers({
  data: dataReducer,
  modal: modalReducer,
});

export default reducer;
