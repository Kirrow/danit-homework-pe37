import React, { useState, useEffect } from "react";
import "./App.css";
import Modal from "./components/Modal/Modal";
import GoodsContainer from "./components/GoodsContainer/GoodsContainer";
import PropTypes from "prop-types";
import Header from "./components/Header/Header";
// import HomePage from "./pages/HomePage/HomePage";
import Routes from "./Routes/Routes";
import { Provider } from "react-redux";
import store from "./store/index";

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <Header />
        <Modal />
        <section>
          {/* <GoodsContainer /> */}
          <Routes />
        </section>
      </div>
    </Provider>
  );
}

export default App;
