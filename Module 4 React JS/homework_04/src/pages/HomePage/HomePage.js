import React, { useEffect } from "react";
import Good from "../../components/Good/Good";
import styles from "./HomePage.module.scss";
import { useDispatch, useSelector } from "react-redux";
import { fetchData } from "../../store/actionCreators/dataAC";

const HomePage = () => {
  const goods = useSelector(({ data }) => data.data);

  console.log(goods);
  const isFavorite = useSelector(({ data }) => data.isFavorite);
  // const isLoading = useSelector(({ notes }) => notes.isLoading);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchData());
  }, []);

  // console.log(goods);

  if (!goods)
    return <p style={{ fontSize: 24 }}>You don't have any vegetables yet</p>;

  return (
    <section className={styles.root}>
      <h1 className={styles.pageTitle}>GOODS</h1>
      <div className={styles.HomePage}>
        {goods &&
          goods.map(({ id, isFavorite, isInCart, ...args }) => (
            <Good
              key={id}
              id={id}
              isFavorite={isFavorite}
              isInCart={isInCart}
              {...args}
            />
          ))}
      </div>
    </section>
  );
};

// HomePage.propTypes = {
//   goods: PropTypes.array,
//   openModal: PropTypes.func,
//   setIsFavorite: PropTypes.func,
//   setIsNotFavorite: PropTypes.func,
//   changeId: PropTypes.func,
// };

// HomePage.propTypes = {
//   goods: [],
//   openModal: () => {},
//   setIsFavorite: () => {},
//   setIsNotFavorite: () => {},
//   changeId: () => {},
// };

export default HomePage;
