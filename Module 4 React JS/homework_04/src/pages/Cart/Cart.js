import React from "react";
import styles from "./Cart.module.scss";
import CartContainer from "../../components/CartContainer/CartContainer";

const Cart = props => {
  //   console.log(props);
  // const { goods, openModal, changeId } = props;
  //   console.log(goods);

  return (
    <>
      <h1 className={styles.pageTitle}>Cart</h1>
      <CartContainer />
    </>
  );
};

export default Cart;
