import React from "react";
import CartItem from "../CartItem/CartItem";
import styles from "./CartContainer.module.scss";
import { useSelector } from "react-redux";

const CartContainer = () => {

  // const dispatch = useDispatch()
  const goods = useSelector(({ data }) => data.data);
  console.log(goods);

  // const { goods, openModal, changeId } = props;
  return (
    <div className={styles.cartContainer}>
      {goods.map(({ id, isInCart, ...args }) => {
        if (isInCart) {
          return (
            <CartItem
              key={id}
              id={id}
              isInCart={isInCart}
              {...args}
            />
          );
        }
      })}
    </div>
  );
};

export default CartContainer;
