import React, { useState, useEffect } from "react";
import "./App.css";
import Modal from "./components/Modal/Modal";
import GoodsContainer from "./components/GoodsContainer/GoodsContainer";
import PropTypes from "prop-types";
import Header from "./components/Header/Header";
import HomePage from "./pages/HomePage/HomePage";
import Routes from "./Routes/Routes";

function App() {
  const [data, setData] = useState([]);
  const [isOpenModalAddToCart, setIsOpenModalAddToCart] = useState(false);
  const [isOpenModalDeleteFromCart, setIsOpenModalDeleteFromCart] =
    useState(false);
  const [id, setId] = useState("");

  const openModal = (isOpenModalAddToCart = false) => {
    // console.log('work openModal');
    if (isOpenModalAddToCart) {
      setIsOpenModalAddToCart(true);
    } else {
      setIsOpenModalDeleteFromCart(true);
    }
  };

  const closeModal = () => {
    setIsOpenModalAddToCart(false);
    setIsOpenModalDeleteFromCart(false);
  };

  const fetchData = async () => {
    const dataInLocalStorage = localStorage.getItem("data");
    const data = JSON.parse(dataInLocalStorage);
    if (data) {
      setData(data);
    } else {
      const result = await fetch("./goods.json").then(res => res.json());
      localStorage.setItem("data", JSON.stringify(result));
      setData(result);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const setIsFavorite = id => {
    // console.log(id);
    setData(prev => {
      const index = prev.findIndex(item => item.id === id);
      if (index === -1) {
        return [...prev];
      } else {
        const newData = [...prev];
        newData[index].isFavorite = true;
        localStorage.setItem("data", JSON.stringify(newData));
        return newData;
      }
    });
  };
  const setIsNotFavorite = id => {
    // console.log(id);
    setData(prev => {
      const index = prev.findIndex(item => item.id === id);
      if (index === -1) {
        return [...prev];
      } else {
        const newData = [...prev];
        newData[index].isFavorite = false;
        localStorage.setItem("data", JSON.stringify(newData));
        return newData;
      }
    });
  };

  const addToCart = id => {
    // console.log(id);
    setData(prev => {
      const index = prev.findIndex(item => item.id === id);
      // console.log(index);
      if (index === -1) {
        return [...prev];
      } else {
        const newData = [...prev];
        newData[index].isInCart = true;
        localStorage.setItem("data", JSON.stringify(newData));
        return newData;
      }
    });
    closeModal();
  };

  const deleteFromCart = id => {
    // console.log(id);
    setData(prev => {
      const index = prev.findIndex(item => item.id === id);
      // console.log(index);
      if (index === -1) {
        return [...prev];
      } else {
        const newData = [...prev];
        newData[index].isInCart = false;
        localStorage.setItem("data", JSON.stringify(newData));
        return newData;
      }
    });
    closeModal();
  };

  const changeId = newId => {
    // console.log('work change Id');
    setId(newId);
  };

  const modalAddToCart = {
    header: "Do you really want to add this item to cart?",
    closeButton: true,
    text: "Later you can delete this item from Cart. Are you sure you want to add it to CART?",
  };
  const modalDeleteFromCart = {
    header: "Do you really want to delete this item from Cart?",
    closeButton: true,
    text: "Later you can add this item to Cart again. Are you sure?",
  };

  return (
    <>
      <Header />
      {isOpenModalAddToCart && (
        <div className="modal">
          <div onClick={() => closeModal()} className="background" />
          <Modal
            header={modalAddToCart.header}
            closeButton={modalAddToCart.closeButton}
            text={modalAddToCart.text}
            isModalOpen={isOpenModalAddToCart}
            closeModal={() => closeModal()}
            addToCart={() => addToCart(id)}
            id={id}
            className="modalAddToCart"
          />
        </div>
      )}
      {isOpenModalDeleteFromCart && (
        <div className="modal">
          <div onClick={() => closeModal()} className="background" />
          <Modal
            header={modalDeleteFromCart.header}
            closeButton={modalDeleteFromCart.closeButton}
            text={modalDeleteFromCart.text}
            isModalOpen={isOpenModalDeleteFromCart}
            closeModal={() => closeModal()}
            deleteFromCart={() => deleteFromCart(id)}
            id={id}
            className="modalDeleteFromCart"
          />
        </div>
      )}
      <Routes
        goods={data}
        setIsFavorite={setIsFavorite}
        setIsNotFavorite={setIsNotFavorite}
        changeId={changeId}
        openModal={openModal}
      />
    </>
  );
}

App.propTypes = {
  isModalOpen: PropTypes.bool,
  data: PropTypes.array,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

App.defaultProps = {
  isModalOpen: false,
  data: [],
  id: "",
};

export default App;
