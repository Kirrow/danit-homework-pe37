import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import Cart from "../pages/Cart/Cart";
import Favorites from "../pages/Favorites/Favorites";
import HomePage from "../pages/HomePage/HomePage";

const Routes = props => {
  const { goods, setIsFavorite, setIsNotFavorite, changeId, openModal } = props;
  return (
    <Switch>
      <Route exact path="/cart">
        <Cart goods={goods} openModal={openModal} changeId={changeId} />
      </Route>

      <Route exact path="/favorites">
        <Favorites
          goods={goods}
          setIsNotFavorite={setIsNotFavorite}
          changeId={changeId}
        />
      </Route>
      <Route exact path="/">
        <HomePage
          goods={goods}
          setIsFavorite={setIsFavorite}
          setIsNotFavorite={setIsNotFavorite}
          changeId={changeId}
          openModal={openModal}
        />
      </Route>
      <Route exact path="/home">
        <Redirect to="/" />
      </Route>
    </Switch>
  );
};

export default Routes;
