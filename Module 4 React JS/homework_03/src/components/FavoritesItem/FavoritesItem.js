import React from "react";
import styles from "./FavoritesItem.module.scss";
import { ReactComponent as StarPlus } from "../../assets/svg/starPlus.svg";
import { ReactComponent as Star } from "../../assets/svg/star.svg";
// import PropTypes from "prop-types";
// import Button from "../Button/Button";
// import { ReactComponent as StarPlus } from "../../assets/svg/starPlus.svg";
// import { ReactComponent as Star } from "../../assets/svg/star.svg";

const FavoritesItem = props => {
  // console.log(props);
  const { id, name, price, article, url, setIsNotFavorite } = props;

  return (
    <div className={styles.root}>
      <div className={styles.svg__wrapper}><Star onClick={() => setIsNotFavorite(id)} /></div>
      <div className={styles.goodImgWrapper}>
        <img src={url} alt={name} className={styles.goodImg} />
      </div>
      <h1 className={styles.name}>{name}</h1>
      <div className={styles.priceWrapper}>
        <h3>Цена: {price} грн</h3>
      </div>
      <p>Номер товара: {article}</p>
    </div>
  );
};

// Good.propTypes = {
//   id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
//   name: PropTypes.oneOfType([
//     PropTypes.string,
//     PropTypes.number,
//     PropTypes.symbol,
//   ]),
//   price: PropTypes.number.isRequired,
//   url: PropTypes.string,
//   article: PropTypes.number,
//   isFavorite: PropTypes.bool,
//   openModal: PropTypes.func,
//   setIsFavorite: PropTypes.func,
//   isInCart: PropTypes.bool,
//   changeId: PropTypes.func,
// };

// Good.defaultProps = {
//   id: "",
//   name: "",
//   price: "",
//   url: "",
//   article: "",
//   isFavorite: false,
//   openModal: () => {},
//   setIsFavorite: () => {},
//   isInCart: false,
//   changeId: () => {},
// };

export default FavoritesItem;
