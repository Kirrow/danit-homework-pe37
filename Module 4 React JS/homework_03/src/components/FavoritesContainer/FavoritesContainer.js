import React from "react";
import FavoritesItem from "../FavoritesItem/FavoritesItem";
import styles from "./FavoritesContainer.module.scss";

const FavoritesContainer = props => {
  // console.log(props);

  const { goods, setIsNotFavorite, changeId } = props;
  return (
    <div className={styles.favoritesContainer}>
      {goods.map(({ id, isFavorite, ...args }) => {
        if (isFavorite) {
          return (
            <FavoritesItem
              key={id}
              setIsNotFavorite={setIsNotFavorite}
              changeId={changeId}
              id={id}
            //   isInCart={isInCart}
              {...args}
            />
          );
        }
      })}
    </div>
  );
};

export default FavoritesContainer;
