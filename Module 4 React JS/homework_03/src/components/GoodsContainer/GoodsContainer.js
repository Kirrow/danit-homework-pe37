import React, { PureComponent } from "react";
import Good from "../Good/Good";
import styles from "./GoodsContainer.module.scss";
import PropTypes from "prop-types";

const GoodsContainer = props => {
  const { goods, setIsFavorite, setIsNotFavorite, isInCart, changeId, openModal } = props;

  return (
    <section className={styles.root}>
      <h1>GOODS</h1>
      <div className={styles.GoodsContainer}>
        {goods.map(({ id, isFavorite, ...args }) => (
          <Good
            key={id}
            id={id}
            setIsFavorite={setIsFavorite}
            setIsNotFavorite={setIsNotFavorite}
            isFavorite={isFavorite}
            isInCart={isInCart}
            changeId={changeId}
            openModal={openModal}
            {...args}
          />
        ))}
      </div>
    </section>
  );
};

GoodsContainer.propTypes = {
  goods: PropTypes.array,
  openModal: PropTypes.func,
  setIsFavorite: PropTypes.func,
  setIsNotFavorite: PropTypes.func,
  isInCart: PropTypes.bool,
  changeId: PropTypes.func,
};

GoodsContainer.propTypes = {
  goods: [],
  openModal: () => {},
  setIsFavorite: () => {},
  setIsNotFavorite: () => {},
  isInCart: false,
  changeId: () => {},
};

export default GoodsContainer;
