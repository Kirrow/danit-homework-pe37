import React from "react";
import CartItem from "../CartItem/CartItem";
import styles from "./CartContainer.module.scss";

const CartContainer = props => {
  // console.log(props);

  const { goods, openModal, changeId } = props;
  return (
    <div className={styles.cartContainer}>
      {goods.map(({ id, isInCart, ...args }) => {
        if (isInCart) {
          return (
            <CartItem
              key={id}
              openModal={openModal}
              changeId={changeId}
              id={id}
              isInCart={isInCart}
              // changeId={changeId}
              {...args}
            />
          );
        }
      })}
    </div>
  );
};

export default CartContainer;
