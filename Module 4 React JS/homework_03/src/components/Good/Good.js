import React, { PureComponent } from "react";
import styles from "./Good.module.scss";
import PropTypes from "prop-types";
import Button from "../Button/Button";
import { ReactComponent as StarPlus } from "../../assets/svg/starPlus.svg";
import { ReactComponent as Star } from "../../assets/svg/star.svg";

const Good = props => {
  const {
    id,
    name,
    price,
    url,
    article,
    isFavorite,
    setIsFavorite,
    setIsNotFavorite,
    isInCart,
    openModal,
    changeId,
  } = props;

  return (
    <div className={styles.root}>
      {isFavorite ? (
        <div className={styles.svg__wrapper}><div><Star onClick={() => setIsNotFavorite(id)} /></div></div>
      ) : (
        <div className={styles.svg__wrapper}><div><StarPlus onClick={() => setIsFavorite(id)} /></div></div>
      )}
      <div className={styles.goodImgWrapper}>
        <img src={url} alt={name} className={styles.goodImg} />
      </div>
      <h1 className={styles.name}>{name}</h1>
      <div className={styles.priceWrapper}>
        <h3>Цена: {price} грн</h3>
        <div className={styles.btn__addCartWrapper}><Button
          onClick={() => {
            changeId(id);
            openModal(true);
          }}
          backgroundColor="rgb(255, 243, 177)"
          text="ADD to CART"
          id={id}
          className={styles.btn__addCart}
        /></div>
      </div>
      <p>Номер товара: {article}</p>
      {isInCart ? <span>Is in Cart</span> : <span>Not in Cart</span>}
    </div>
  );
};

Good.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  name: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.symbol,
  ]),
  price: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  url: PropTypes.string,
  article: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  isFavorite: PropTypes.bool,
  openModal: PropTypes.func,
  setIsFavorite: PropTypes.func,
  isInCart: PropTypes.bool,
  changeId: PropTypes.func,
};

Good.defaultProps = {
  id: "",
  name: "",
  price: "",
  url: "",
  article: "",
  isFavorite: false,
  openModal: () => {},
  setIsFavorite: () => {},
  isInCart: false,
  changeId: () => {},
};

export default Good;
