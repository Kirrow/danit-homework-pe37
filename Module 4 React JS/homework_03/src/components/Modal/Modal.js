import React, { Component } from "react";
import styles from "./modal.scss";
import PropTypes from "prop-types";

const Modal = props => {
  const {
    header,
    closeButton,
    text,
    closeModal,
    className,
    addToCart,
    id,
    deleteFromCart,
  } = props;

  return (
    <>
      <div className="modal" onClick={() => closeModal()}>
        <div className={className} onClick={e => e.stopPropagation()}>
          <div className="modal__header">
            <h1 className="modal__header--title">{header}</h1>
            {closeButton && (
              <button
                className="modal__header--close-btn"
                onClick={() => closeModal()}
              >
                X
              </button>
            )}
          </div>
          <p className="modal__text">{text}</p>
          <div className="modal__button-container">
            {addToCart && (
              <button
                className="modal__button modal1__button modal1__button--first"
                onClick={() => addToCart(id)}
              >
                ADD
              </button>
            )}
            {deleteFromCart && (
              <button
                className="modal__button modal1__button modal1__button--first"
                onClick={() => deleteFromCart(id)}
              >
                DELETE FROM CART
              </button>
            )}
            <button
              className="modal__button modal1__button modal1__button--second"
              onClick={() => closeModal()}
            >
              CANCEL
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

Modal.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  header: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.symbol,
  ]),
  closeButton: PropTypes.bool,
  text: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.symbol,
  ]),
  closeModal: PropTypes.func,
  className: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  addToCart: PropTypes.func,
};

export default Modal;
