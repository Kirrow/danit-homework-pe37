import React, { PureComponent } from "react";
import styles from "./button.scss";
import PropTypes from "prop-types";

const Button = props => {
  const { backgroundColor, text, id, changeId, openModal, onClick, className } = props;
  return (
    <>
      <button
        style={{ backgroundColor: backgroundColor }}
        onClick={() => onClick()}
        className="buttons__main"
      >
        {text}
      </button>
    </>
  );
};

Button.propTypes = {
  backgroundColor: PropTypes.string.isRequired,
  text: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  changeId: PropTypes.func,
};

Button.defaultProps = {
  backgroundColor: "white",
  text: "",
  id: "",
  changeId: () => {},
};

export default Button;
