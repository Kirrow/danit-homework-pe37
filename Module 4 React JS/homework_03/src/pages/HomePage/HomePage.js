import React from "react";
import Good from "../../components/Good/Good";
import styles from "./HomePage.module.scss";
import PropTypes from "prop-types";

const HomePage = props => {
    // console.log(props);
  const { goods, setIsFavorite, setIsNotFavorite, changeId, openModal } = props;

  return (
    <section className={styles.root}>
      <h1 className={styles.pageTitle}>GOODS</h1>
      <div className={styles.HomePage}>
        {goods.map(({ id, isFavorite, isInCart, ...args }) => (
          <Good
            key={id}
            id={id}
            setIsFavorite={setIsFavorite}
            setIsNotFavorite={setIsNotFavorite}
            isFavorite={isFavorite}
            isInCart={isInCart}
            changeId={changeId}
            openModal={openModal}
            {...args}
          />
        ))}
      </div>
    </section>
  );
};

HomePage.propTypes = {
  goods: PropTypes.array,
  openModal: PropTypes.func,
  setIsFavorite: PropTypes.func,
  setIsNotFavorite: PropTypes.func,
  changeId: PropTypes.func,
};

HomePage.propTypes = {
  goods: [],
  openModal: () => {},
  setIsFavorite: () => {},
  setIsNotFavorite: () => {},
  changeId: () => {},
};

export default HomePage;