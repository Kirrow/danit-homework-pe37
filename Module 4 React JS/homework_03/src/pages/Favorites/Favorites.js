import React from "react";
import styles from "./Favorites.module.scss";
import FavoritesContainer from "../../components/FavoritesContainer/FavoritesContainer";

const Favorites = props => {
  const { goods, changeId, setIsNotFavorite } = props;

  return (
    <>
      <h1 className={styles.pageTitle}>Favorites</h1>
      <FavoritesContainer goods={goods} setIsNotFavorite={setIsNotFavorite} changeId={changeId} />
    </>
  );
};

export default Favorites;

