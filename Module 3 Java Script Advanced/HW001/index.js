//  Задание
//  Реализовать класс Employee, в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата). Сделайте так, чтобы эти свойства заполнялись при создании объекта.
//  Создайте геттеры и сеттеры для этих свойств.
//  Создайте класс Programmer, который будет наследоваться от класса Employee, и у которого будет свойство lang (список языков).
//  Для класса Programmer перезапишите геттер для свойства salary. Пусть он возвращает свойство salary, умноженное на 3.
//  Создайте несколько экземпляров обьекта Programmer, выведите их в консоль.

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get name() {
    this._name;
  }
  set name(value) {
    this._name = value;
  }

  get age() {
    return this._age;
  }
  set age(value) {
    this._age = value;
  }
  get salary() {
    return this._salary;
  }
  set salary(value) {
    this._salary = value;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  set salary(value) {
    this._salary = value;
  }
  get salary() {
    return this._salary * 3;
  }
}

const testEmployee = new Employee("Employee1", "25", "20000");
const testProgrammer = new Programmer(
  "Programmer",
  "28",
  "15000",
  "UA, EN, RU"
);
const programmer1 = new Programmer("Programmer1", "27", "18000", "UA, IT, EN");
const programmer2 = new Programmer("Programmer2", "20", "25000", "EN");

console.log(testEmployee);
console.log(testEmployee.salary);
console.log(testProgrammer);
console.log(testProgrammer.salary);
console.log(programmer1);
console.log(programmer1.salary);
console.log(programmer2);
console.log(programmer2.salary);
