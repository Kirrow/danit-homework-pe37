// Выведите этот массив на экран в виде списка (тег ul - список должен быть сгенерирован с помощью Javascript).
// На странице должен находиться div с id="root", куда и нужно будет положить этот список
// (похожая задача была дана в модуле basic).
// Перед выводом обьекта на странице, нужно проверить его на корректность (в объекте должны содержаться все
// три свойства - author, name, price). Если какого-то из этих свойств нету, в консоли должна высветиться ошибка
// с указанием - какого свойства нету в обьекте.
// Те элементы массива, которые являются некорректными по условиям предыдущего пункта, не должны появиться на странице.

const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

const checkBookReq = books.map(function (author, name, price) {
  try {
    if (Array.isArray(books)) {
      const listTemplate = books.reduce((acc, el, index) => {
        if (index === 0) acc += "<ul>";
        // debugger;
        if (
          el.hasOwnProperty("author") &&
          el.hasOwnProperty("name") &&
          el.hasOwnProperty("price")
        ) {
          let str = "";
          for (let key in el) {
            str += key + ": " + el[key] + "; ";
          }
          acc += `<li>${str}</li>`;
        } else if (!el.author) {
          throw new Error(`Book ${el.name} do not include author`);
        } else if (!el.name) {
          throw new Error(`Book do not include name`);
        } else if (!el.price) {
          throw new Error(`Book ${el.name} do not include price`);
        }
        // debugger;
        if (index === books.length - 1) acc += "</ul>";
        return acc;
      }, "");
      return (document.querySelector("#root").innerHTML = listTemplate);
    }
  } catch (err) {
    console.log(err);
  }
});
