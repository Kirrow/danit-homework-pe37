const getIp = async function () {
  try {
    const response = await fetch("https://api.ipify.org/?format=json");

    if (response.status >= 200 && response.status < 300) {
      const ipObj = await response.json();
      // console.log(ipObj);
      return ipObj;
    }
    throw new Error(response.status);
  } catch (error) {
    throw new Error(error);
  }
};

function getAddress(ip) {
  return fetch(`http://ip-api.com/json/${ip}`).then(response =>
    response.json()
  );
}

async function renderAddress() {
  const ipObj = await getIp();
  const { ip } = ipObj;
  // console.log(ip);
  const objAddress = await getAddress(ip);
  const {
    continent = "Not getted",
    country = "Not getted",
    regionName = "Not getted",
    city = "Not getted",
    district = "Not getted",
  } = objAddress;
  const wrapper = document.getElementById("root");
  wrapper.insertAdjacentHTML(
    "beforeend",
    `
                    <div>
                    <h2> Address of IP: ${ip}</h2>
                        <p>Continent: ${continent}</p>
                        <p>Country: ${country}</p>
                        <p>Region: ${regionName}</p>
                        <p>City: ${city}</p>
                        <p>District of city: ${district}</p>
                    </div>
                    `
  );
}

const btn = document.getElementById("btn");
btn.addEventListener("click", renderAddress);
