const gulp = require("gulp"),
  sass = require("gulp-sass")(require("sass")),
  autoprefixer = require("gulp-autoprefixer"),
  concat = require("gulp-concat"),
  rename = require("gulp-rename"),
  minifyjs = require("gulp-js-minify"),
  cleanCSS = require("gulp-clean-css"),
  watch = require("gulp-watch"),
  browserSync = require("browser-sync").create(),
  imagemin = require("gulp-imagemin"),
  clean = require("gulp-clean");
//   concat = require('gulp-concat'),

const paths = {
  html: "./index.html",
  src: {
    scss: "./src/scss/**/*.scss",
    js: "./src/js/*.js",
    img: "./src/img/**/*.png",
  },
  build: {
    css: "./dist/css/",
    js: "./dist/js/",
    img: "./dist/img/",
    self: "./dist/",
  },
};

const buildCssDev = () =>
  gulp
    .src(paths.src.scss)
    .pipe(sass().on("error", sass.logError))
    // .pipe(cleanCSS({ level: 2 }))
    .pipe(rename("styles.min.css"))
    .pipe(gulp.dest(paths.build.css))
    .pipe(browserSync.stream());

const buildCss = () =>
  gulp
    .src(paths.src.scss)
    .pipe(sass().on("error", sass.logError))
    .pipe(
      autoprefixer({
        browsers: ["last 3 versions"],
        cascade: false,
      })
    )
    .pipe(cleanCSS({ level: 2 }))
    .pipe(rename("styles.min.css"))
    .pipe(gulp.dest(paths.build.css));

const buildJSDev = () =>
  gulp
    .src(paths.src.js)
    .pipe(concat("scripts.min.js"))
    .pipe(minifyjs())
    .pipe(gulp.dest(paths.build.js))
    .pipe(browserSync.stream());

const buildJS = () =>
  gulp
    .src(paths.src.js)
    .pipe(concat("scripts.min.js"))
    .pipe(minifyjs())
    .pipe(gulp.dest(paths.build.js));

const buildIMG = () =>
  gulp.src(paths.src.img).pipe(imagemin()).pipe(gulp.dest(paths.build.img));

const cleanBuild = () =>
  gulp.src(paths.build.self, { allowEmpty: true }).pipe(clean());

const watcher = () => {
  browserSync.init({
    server: {
      baseDir: "./",
    },
  });

  gulp.watch(paths.src.scss, buildCssDev).on("change", browserSync.reload);
  gulp.watch(paths.src.js, buildJS).on("change", browserSync.reload);
  gulp.watch(paths.html).on("change", browserSync.reload);
  // gulp.watch(paths.src.img, buildIMG).on("change", browserSync.reload);
};

gulp.task(
  "dev",
  gulp.series(cleanBuild, gulp.parallel(buildCssDev, buildJSDev, buildIMG), watcher)
);
gulp.task(
  "build",
  gulp.series(cleanBuild, gulp.parallel(buildCss, buildJS, buildIMG))
);
