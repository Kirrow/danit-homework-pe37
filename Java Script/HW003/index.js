function mathAction (firstNum, secondNum, operation) {
	switch(operation) {
		case '+':
			return firstNum + secondNum;
		case '-':
			return firstNum - secondNum;
		case '*':
			return firstNum * secondNum;
		case '/':
			return firstNum / secondNum;
	}
}
let firstNum;
let secondNum;

do {firstNum = +prompt('Введите первое число');
secondNum = +prompt('Введите второе число')
} while (firstNum == "" || firstNum === undefined || firstNum === null || isNaN(firstNum) || secondNum == "" || secondNum === null || isNaN(secondNum) || secondNum === undefined);

let operation = prompt('Введите математическое действие', '+, -, *, /')
while (operation !== "+" && operation !== "-" && operation !== "/" && operation !== "*") {
	operation = prompt('Введите корректное математическое действие с числами');
}

let result = mathAction(firstNum, secondNum, operation);
console.log(result);
