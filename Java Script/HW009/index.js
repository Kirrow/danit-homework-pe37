
    const ulTabs = document.querySelectorAll('.tabs-title')
    const tabTexts = document.querySelectorAll('.list');

    // console.log(ulTabs);
    // console.log(tabTexts);

ulTabs.forEach(elem => {
    elem.addEventListener('click', function(event){
        document.querySelector('.tabs li.active').classList.remove("active");
        event.target.classList.add("active");

        const textIndex = event.target.getAttribute('data-title')
        console.log(textIndex);

        tabTexts.forEach(elem => {
            // debugger
            elem.setAttribute("hidden", true);
            if (elem.getAttribute('data-text') === textIndex){
                elem.removeAttribute("hidden");
            }
        })
    })
})
