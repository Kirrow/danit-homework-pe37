const imgs = document.querySelectorAll('.image-to-show')
const btnStop = document.getElementById('btnStop');
const btnContinue = document.getElementById('btnContinue')
const timer = document.getElementById('timer')
const timeToNextImgConst = 3000;

btnStop.addEventListener('click', pause)

function pause(){
clearInterval(interval);
}

let interval = startInterval();
btnContinue.addEventListener('click', function(){
    interval = startInterval();
})

// console.log(imgs);
let index = 0
function showImg (){
    imgs[index].setAttribute('hidden', true)
    index++
    if (index === imgs.length) {
        index = 0
    }
    imgs[index].removeAttribute('hidden')
}

let timeToNextImg = timeToNextImgConst;
function startInterval(){
   return setInterval(()=>{
        timeToNextImg -=10
        if (timeToNextImg <=0){
            timeToNextImg = timeToNextImgConst;
            showImg()
        }
        timer.innerText = (timeToNextImg / 1000).toFixed(3);
    }, 10)
}
