Домашнее задание №11. Теоретический вопрос.
1. Опишите своими словами разницу между функциями setTimeout() и setInterval().
Функция SetTimeout запускает код один раз через определенное время, установленное в SetTimeout.
Функция SetInterval запускает код бесконечное количество раз через определенное время, установленное в SetInterval до момента ее отмены.
2. Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему?
Функция с нулевой задержкой сначала подождет, пока выполнится остальной код, а только после этого выполнится код в функции.
3. Почему важно не забывать вызывать функцию clearInterval(), когда ранее созданный цикл запуска вам уже не нужен?
SetInterval функция остаётся в памяти до тех пор, пока не будет вызван clearInterval.

Функция ссылается на внешние переменные, которые существуют тоже, пока существует функция. Такие переменные могут занимать больше памяти, чем сама функция. Поэтому, если цикл запуска больше не нужен, то лучше отменить его.
