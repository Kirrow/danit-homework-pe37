const iconEyes = document.querySelector('fas');
const inputPassword = document.getElementById('inputPassword');
const inputPasswordConfirm = document.getElementById('inputPasswordConfirm');
const iconEyePassword = document.getElementById('iconEyePassword');
const iconEyePasswordConfirm = document.getElementById('iconEyePasswordConfirm');
const btn = document.getElementById('btn');


iconEyePassword.addEventListener('click', showPassword);
function showPassword(){
    // debugger
    iconEyePassword.classList.toggle('fa-eye-slash')
    if (iconEyePassword.classList.contains('fa-eye-slash')){
        inputPassword.setAttribute('type', 'text');
    }else{
        inputPassword.setAttribute('type', 'password');
    }
}

iconEyePasswordConfirm.addEventListener('click', showPasswordConfirm);
function showPasswordConfirm(){
    // debugger
    iconEyePasswordConfirm.classList.toggle('fa-eye-slash')
    if (iconEyePasswordConfirm.classList.contains('fa-eye-slash')){
        inputPasswordConfirm.setAttribute('type', 'text');
    }else{
        inputPasswordConfirm.setAttribute('type', 'password');
    }
}

const span = document.createElement('span');
inputPasswordConfirm.after(span);
btn.addEventListener('click', function(event){
    event.preventDefault();
    if (inputPassword.value === inputPasswordConfirm.value){
        span.textContent = '';
        alert('You are welcome!!!');
    }else{
        span.innerText = `Нужно ввести одинаковые значения`;
        span.style.color = 'red';
    }
})
