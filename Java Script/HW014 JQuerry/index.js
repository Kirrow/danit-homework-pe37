
$(document).ready(function() {
  const button = $('#button-up');
  $(window).scroll (function () {
    if ($(this).scrollTop () > 1000) {
      button.fadeIn();
    } else {
      button.fadeOut();
    }
});
button.on('click', function(){
$('body, html').animate({
scrollTop: 0
}, 800);
return false;
});
});

$(document).on("click", "#nav-menu-sections-wrapper a", function(e) {
    e.preventDefault();
    const id  = $(this).attr('href');
    const top = $(id).offset().top;
    $('body, html').animate({scrollTop: top}, 800);
});


$('#hide-section').on('click', function(){
    $( "#posts" ).slideToggle( "slow" );
})
