const button = document.getElementById('changeTheme')
const formContainer = document.querySelector('.container')
const main = document.getElementById('main')
const header = document.querySelector('#header')

console.log(header);

button.addEventListener('click', setDarkThemeLocalStorage)


function setDarkThemeLocalStorage(){
        if(document.body.classList.contains('darkTheme')){
            document.body.classList.remove('darkTheme')
            formContainer.classList.remove('darkTheme');
            header.classList.remove('darkTheme');
            localStorage.setItem("theme", "none");
        }else{
            console.log('Кнопка нажата!');
            document.body.classList.add('darkTheme');
            formContainer.classList.add('darkTheme');
            header.classList.add('darkTheme');
            localStorage.setItem("theme", "darkTheme");
        }
    }

    function oldLocalStorage() {
        if (localStorage.getItem("theme") !== null) {
            let oldTheme = localStorage.getItem("theme");
            if (oldTheme === "darkTheme") {
            document.body.classList.add('darkTheme');
            formContainer.classList.add('darkTheme');
            header.classList.add('darkTheme');
            }
        }
    }

    oldLocalStorage()
