
const inputPrice = document.querySelector('#price');
const span = document.createElement('p');
document.body.prepend(span)
const btn = document.createElement('button')
btn.textContent = 'x';
const textWrongNumber = document.createElement('p');
document.body.append(textWrongNumber)

inputPrice.addEventListener('focus', function(){
    inputPrice.style.border = '5px solid green'})

inputPrice.addEventListener('blur', function(){
    if (inputPrice.value > 0) {
        span.textContent = `Текущая цена: ${this.value}`;
        span.append(btn)
        inputPrice.style.color = 'green';
        textWrongNumber.textContent = ''
    }else{
        inputPrice.style.border = '5px solid red';
        textWrongNumber.textContent = 'Please enter correct price';
        span.textContent = '';
    }
})

btn.addEventListener("click", function () {
    span.textContent = '';
    btn.remove();
    inputPrice.value = '';

  })
