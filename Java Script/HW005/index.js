
let newUser = {};
function createNewUser(firstName, lastName, birthday){
	firstName = prompt('Введите Ваше имя.');
	lastName = prompt('Введите Вашу фамилию.');
	birthday = prompt('Введите Вашу дату рождения.', 'dd.mm.yyyy');
	let re = /[0-3][0-9]\.[0-1][0-9]\.[1][9]\d\d/;
	while (re.test(birthday) === false) {
		birthday = prompt('Введите корректную дату Вашего рождения.', 'dd.mm.yyyy');
		};
		birthday = new Date(...birthday.split('.').reverse());
		newUser = {
		firstName,
		lastName,
		birthday,
		getLogin () {
			return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
		},

		getAge () {
			let yearOfBirth = this.birthday.getFullYear();
			let years = (new Date()).getFullYear() - yearOfBirth
			return years;
		},

		getPassword(){
		return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear();
	},
}
	return newUser;
	};
let result = createNewUser();
console.log(result.getAge());
console.log(result.getLogin());
console.log(result.getPassword());
